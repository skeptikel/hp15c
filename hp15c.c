/* 
 * Course:  CoSc 211
 * Term:    Winter 2015
 * Lab:     2 - Writing an HP15C calculator in C
 * File:    hp15c.c
 * Author:  Jason Sayler
 */

/*
 * EXIT CODES:
 *      1   File could not be found or opened.
 *      2   Reached end of memory (key-presses) before RTN was found.
 */

#include <stdlib.h>
#include <stdio.h>

char *mem;
double *stack;
double *reg;

char *pc;
double *top = NULL;
char *end;

void readFile(char *fileName);
void push(double n);
double pop();
double getDigits();
void operator();
void f();
void g();

/**
 * 
 * @param argc The number of arguments, INCLUDING the executable (never 0)
 * @param argv A 2D array containing arguments
 * @return int that is 0 on clean exit, other on error.
 */
int main(int argc, char* argv[]) {
    char *fileName;

    // Allocate: Memory, Stack, Register Set
    mem = (char*) malloc(100 * sizeof (char));
    stack = (double*) malloc(25 * sizeof (double));
    reg = (double*) malloc(20 * sizeof (double));

    end = mem;
    pc = mem;
    // If no filename given, use a default.
    if (argc > 1) {
        fileName = argv[1];
    } else {
        fileName = "prog_2pass.txt";
    }
    readFile(fileName);

    printf("\n");
    
    /*
     * This is the top-level loop which runs the hp15c program
     * by iterating through mem[] by means of the pc pointer.
     * 
     * 'pc' initially points to the first element in the mem array,
     * and 'end' points to the element AFTER the last key-press.
     */
    while (pc < end) {
        if (*pc / 10 == 0) {
            push(getDigits());
        } else if (*pc % 10 == 0) {
            operator();
        } else if (*pc == 22) {
            int dest = *++pc;
            pc = mem + dest;
        } else if (*pc == 36) {
            // Enter not implemented, since not in calculator mode.
        } else if (*pc == 42) {
            f();
        } else if (*pc == 43) {
            g();
        } else if (*pc == 44) {
            pc++;
            reg[*pc] = pop();
            pc++;
        } else if (*pc == 45) {
            pc++;
            push(reg[*pc]);
            pc++;
        }
    }
    
    /*
     * If RTN (g 32) was not performed,
     * but the end of instructions was reached,
     * then exit(2).
     */
    exit(2);
    
    printf("\n");

} // end main --------------------------------

/**
 * Reads the file into the mem array
 * @param fileName The name of the desired "program" file.
 */
void readFile(char *fileName) {
    // OPEN the File
    FILE *file;
    if ((file = fopen(fileName, "r")) == NULL) {
        printf("Cannot open file:  %s\n\n", fileName);
        exit(1);
    }

    // Start READING the File
    int x;
    while (fscanf(file, "%d", &x) != EOF) {
        *(end) = (char) x;
        end++;
    }
    fclose(file);
}

void push(double n) {
    if (top == NULL) {
        top = stack;
    } else {
        top++;
    }
    *top = n;
}

double pop() {
    double n = *top;
    top--;
    return n;
}

/**
 * Called only when pc is known to point to a digit.
 * Iterates through mem until sequential digits are read.
 * @return a double holding the value that the digit series specified.
 */
double getDigits() {
    double n = 0;
    while ((*pc) / 10 == 0 && pc < end) {
        n *= 10;
        n += *pc;
        pc++;
    }
    return n;
}

/**
 * Takes an operator that pc points to,
 * pops two numbers from the stack and performs that operation,
 * finally pushing the result back onto the stack.
 */
void operator() {
    double b = pop();
    double a = pop();

    switch (*pc) {
        case 10:
            push(a / b);
            break;
        case 20:
            push(a * b);
            break;
        case 30:
            push(a - b);
            break;
        case 40:
            push(a + b);
            break;
    }
    pc++;
}

void f() {
    pc++;
    if(*pc == 31) {
        printf("%f\n", *top);
    }
    pc++;
}

void g() {
    pc++;
    if(*pc == 20) {
        if(*top != 0.0) {
            pc += 2;
        }
        pc++;
    }
    if(*pc == 32) {
        exit(0);
    }    
}